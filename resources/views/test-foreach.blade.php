@extends('layout/navbar')

@section('title', 'Pemrograman Web Lanjut - A')

    @php
    $users = [
        ['id' => 1, 'name' => 'Alvan'],
        ['id' => 2, 'name' => 'Besar'],
        ['id' => 3, 'name' => 'Kevin']
    ];
    @endphp

@section('container')
    <div class="container">
        <h3>Gaming Group</h3>
        @foreach ($users as $user)
            {{ $user['id'] }}. {{ $user['name'] }}<br>
        @endforeach
    </div>
@endsection
